# LACCHAIN Ethers
Ethers.js v6 compatible LAC-NET Models Provider & Signer

## Community

- [Discord](https://discord.gg/AnGXcZ8P)
- [Code of Conduct](https://xcapit-foss.gitlab.io/documentation/docs/CODE_OF_CONDUCT)
- [Contribution Guideline](https://xcapit-foss.gitlab.io/documentation/docs/contribution_guidelines)

## Getting Started

### Installation

```bash
npm i @xcapit/lacchain-ethers
```

or

```bash
yarn add @xcapit/lacchain-ethers
```

### Usage
```typescript
import { LacchainProvider, LacchainSigner } from '@xcapit/lacchain-ethers';

const provider = new LacchainProvider(RPC_URL);
const signer: LacchainSigner = new LacchainSigner(
  PRIVATE_KEY,
  provider,
  NODE_ADDRESS,
  EXPIRATION
);
```
Where
- RPC_URL: the RPC url of your node (i.e. http://node-ip)
- PRIVATE_KEY: is the ethereum account private key in hex
- NODE_ADDRESS: the node address
- EXPIRATION: the expiration unix timestamp of the transaction

### Examples

#### Deploy smart contract

```typescript
import { ContractFactory } from 'ethers';
import { LacchainProvider, LacchainSigner } from '@xcapit/lacchain-ethers';

const signer: LacchainSigner = new LacchainSigner(
  PRIVATE_KEY,
  new LacchainProvider(RPC_URL!),
  NODE_ADDRESS!,
  EXPIRATION
);

const contractFactory = new ContractFactory(
  CONTRACT_ABI,
  CONTRACT_BYTECODE,
  signer
);

const contract = await contractFactory.deploy(signer.address, TRUSTED_FORWARDER!);
const txReceipt = await contract.deploymentTransaction()?.wait();
console.log( `Contract Address: ${txReceipt?.contractAddress}` );
```

Where:
 - CONTRACT_ABI: is the contract ABI
 - CONTRACT_BYTECODE: is the contract bytecode
 - TRUSTED_FORWARDER: is the trusted forwarder setted by LACChain for the network (Protestnet/Mainnet)

**Note: Don't use contract.address, instead use the example code above to get the contract address**

**Note 2: Find the correct trusted forwarder address in the [LACChain documentation](https://lacnet.com/smart-contracts-overview/)** in the "Adapt your Smart Contract to our GAS Distribution Protocol" section.

#### Invoke and call contract
```typescript
import { Contract } from 'ethers';
import { LacchainProvider, LacchainSigner } from '@xcapit/lacchain-ethers';

const signer: LacchainSigner = new LacchainSigner(
  PRIVATE_KEY,
  new LacchainProvider(RPC_URL!),
  NODE_ADDRESS!,
  EXPIRATION
);

const contract = new Contract(CONTRACT_ADDRESS, CONTRACT_ABI, signer);

await (await contract.someContractFunction()).wait();
```

Where:
- CONTRACT_ADDRESS: is the contract address
- CONTRACT_ABI: is the contract ABI

#### More examples
If you are looking for a complete example in some context go to this e2e test [deploy-and-tx.test.ts](https://gitlab.com/xcapit-foss/lacchain-ethers/-/blob/main/src/tests/e2e/deploy-and-tx.test.ts).
